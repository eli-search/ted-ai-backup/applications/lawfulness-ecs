ARG docker_base_image_url
FROM $docker_base_image_url

RUN apt-get update && apt-get install -y libpq-dev gcc curl

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY app /app
COPY resources /resources

CMD ["python3", "/app/main.py"]
