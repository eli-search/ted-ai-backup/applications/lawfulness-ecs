import re
from decimal import Decimal
from pathlib import Path
from typing import Iterator

from boto3_type_annotations.ssm import Client as SsmClient
from gibberish_detector import detector
from pydantic import BaseModel

from config import WHITELISTED_BODY_CONFIG
from repositories import whitelisted_bodies
from services.extraction import NoticeFields
from utils import logging

LEGAL_TYPES = ["body-pl", "body-pl-cga", "body-pl-la", "body-pl-ra", "cga", "det-cont", "eu-ins-bod-ag", "grp-p-aut",
               "int-org", "la", "org-sub", "org-sub-cga", "org-sub-la", "org-sub-ra", "pub-undert", "pub-undert-cga",
               "pub-undert-la", "pub-undert-ra", "ra", "spec-rights-entity"]
EU_COUNTRY_CODES = ["at", "be", "bg", "by", "cz", "de", "dk", "ee", "es", "fi", "fr", "gr", "hr", "hu", "ie", "it",
                    "lt", "lu", "lv", "mt", "nl", "pl", "pt", "ro", "se", "si", "sk", "eu"]
FORBIDDEN_COUNTRY_CODES = ["ru", "by"]
TEST_WORD_PATTERN = re.compile(r"\btest\b", flags=re.IGNORECASE)
SALE_WORD_PATTERN = re.compile(r"\bsale\b", flags=re.IGNORECASE)
WORD_SEPARATOR_PATTERN = re.compile(r"[^A-Za-z0-9]")

LOGGER = logging.logger(__name__)
GIBBERISH_DETECTOR_MODEL_PATH = Path(__file__).parent.parent.parent / "resources" / "big.model"
GIBBERISH_DETECTOR = detector.create_from_model(str(GIBBERISH_DETECTOR_MODEL_PATH))


class NoticeFieldAnalysis(BaseModel):
    has_test_word: bool
    has_sale_word: bool
    incoherent_texts: list[str]
    is_forbidden_country: bool
    non_eu_countries: list[str]
    is_not_funded_by_eu: bool
    is_contracting_body_private: bool
    is_contracting_body_international: bool
    contracting_body_whitelist_score: Decimal
    is_contracting_body_whitelisted: bool


def run_rules(ssm: SsmClient, notice_id: str, fields: NoticeFields) -> NoticeFieldAnalysis:
    whitelist_score = Decimal(max((
        whitelisted_bodies.whitelist_score(ssm, body)
        for body in fields.contracting_bodies
    ), default=Decimal(0)))
    analysis = NoticeFieldAnalysis(
        has_test_word=any(TEST_WORD_PATTERN.search(text) for text in fields.translated_free_texts),
        has_sale_word=any(SALE_WORD_PATTERN.search(text) for text in fields.translated_free_texts),
        incoherent_texts=[word for text in fields.translated_free_texts for word in _incoherent_words(text)],
        is_forbidden_country=any(country.lower() in fields.countries for country in FORBIDDEN_COUNTRY_CODES),
        non_eu_countries=[country for country in fields.countries if country not in EU_COUNTRY_CODES],
        is_not_funded_by_eu=all(not fund for fund in fields.eu_funds) or 'no-eu-funds' in fields.eu_funds,
        is_contracting_body_private=any(type_.lower() not in LEGAL_TYPES for type_ in fields.buyer_types),
        is_contracting_body_international="int-org" in fields.buyer_types,
        contracting_body_whitelist_score=whitelist_score,
        is_contracting_body_whitelisted=whitelist_score >= WHITELISTED_BODY_CONFIG.min_score,
    )
    LOGGER.debug(f"Computed analysis for notice {notice_id}: {analysis}")
    return analysis


def _incoherent_words(text: str) -> Iterator[str]:
    return (
        word
        for word in re.split(WORD_SEPARATOR_PATTERN, text.lower().strip())
        if (not word.startswith("http")
            and not word.startswith("www")
            and len(word) > 4
            and any(char != 'i' for char in word)
            and GIBBERISH_DETECTOR.is_gibberish(word))
    )
