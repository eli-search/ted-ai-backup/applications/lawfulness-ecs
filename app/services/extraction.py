from typing import Iterator

import country_converter as coco
from pydantic import BaseModel
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM

from repositories.eforms_fields import relative_xpath, absolute_xpath
from services.parsing import Xml
from utils import logging

NOTICE_VERSION_XPATH = absolute_xpath('OPT-002-notice')
NOTICE_ID_XPATH = absolute_xpath('OPP-010-notice')
NOTICE_LANGUAGE_XPATH = absolute_xpath('BT-702(a)-notice')
COMPANY_XPATH = absolute_xpath('ND-Company')
COMPANY_ID_XPATH = relative_xpath('OPT-200-Organization-Company')
BUYER_ID_XPATH = absolute_xpath('OPT-300-Procedure-Buyer')
BUYER_ADDRESS_XPATH = relative_xpath('ND-BusinessAddress')
BUYER_XPATH = f"{COMPANY_XPATH}[{COMPANY_ID_XPATH} = {BUYER_ID_XPATH}]"
BUYER_NAME_XPATH = f"./{relative_xpath('BT-500-Organization-Company')}/text()"
BUYER_TOWN_XPATH = f"./{BUYER_ADDRESS_XPATH}/{relative_xpath('BT-513-Organization-Company')}/text()"
BUYER_POSTAL_CODE_XPATH = f"./{BUYER_ADDRESS_XPATH}/{relative_xpath('BT-512-Organization-Company')}/text()"
BUYER_COUNTRY_XPATH = f"./{BUYER_ADDRESS_XPATH}/{relative_xpath('BT-514-Organization-Company')}/text()"
BUYER_LEGAL_TYPE_XPATH = absolute_xpath('BT-11-Procedure-Buyer')
EU_FUNDS_XPATH = "|".join((
    absolute_xpath('BT-60-Lot'),
    absolute_xpath('BT-6110-Contract'),
    absolute_xpath('BT-6140-Lot'),
    absolute_xpath('BT-722-Contract'),
    absolute_xpath('BT-7220-Lot'),
    absolute_xpath('BT-5011-Contract'),
    absolute_xpath('BT-5010-Lot'),
))
COUNTRY_XPATH = "|".join(("//cbc:CountrySubentityCode", "//cac:Country/cbc:IdentificationCode"))

EXCLUDED_FREE_TEXT_PATH_PARTS = ["ID", "Ident", "Date", "Indicator", "Mail", "URI", "IdentificationCode",
                                 "CountrySubentityCode", "PostalZone", "RegulatoryDomain"]
NOT_FREE_TEXT_XPATHS = ' or '.join(f"contains(name(), '{part}')" for part in EXCLUDED_FREE_TEXT_PATH_PARTS)
OTHER_NOT_FREE_TEXT_XPATHS = "contains(name(parent::*), 'Id')"
FREE_TEXT_XPATH = f"//*[not({NOT_FREE_TEXT_XPATHS} or {OTHER_NOT_FREE_TEXT_XPATHS})]/text()"

BAD_COUNTRY_CODES = ["country", "nuts"]

LOGGER = logging.logger(__name__)


# In multiprocessing context, it is required to created one instance of this class per process to avoid deadlocks.
class TranslationModel:
    def __init__(self):
        self.tokenizer = AutoTokenizer.from_pretrained("Helsinki-NLP/opus-mt-mul-en")
        self.model = AutoModelForSeq2SeqLM.from_pretrained("Helsinki-NLP/opus-mt-mul-en")


class ContractingBody(BaseModel):
    name: str | None
    town: str | None
    postal_code: str | None
    country: str | None


class NoticeFields(BaseModel):
    countries: list[str]
    languages: list[str]
    buyer_types: list[str]
    contracting_bodies: list[ContractingBody]
    eu_funds: list[str]
    free_texts: list[str]
    translated_free_texts: list[str]


def is_eforms_notice(xml: Xml) -> bool:
    version = xml.first(NOTICE_VERSION_XPATH)
    return False if version is None or not version.text else version.text.startswith("eforms")


def extract_notice_id(xml: Xml) -> str | None:
    id_ = xml.first(NOTICE_ID_XPATH)
    return None if id_ is None else id_.text


def extract_fields(notice_id: str, xml: Xml, translation_model: TranslationModel) -> NoticeFields:
    languages = [item.text.lower() for item in xml.all(NOTICE_LANGUAGE_XPATH) if item.text]
    free_texts = _extract_free_text(xml)
    fields = NoticeFields(
        countries=list(set(parse_countries(item.text.lower() for item in xml.all(COUNTRY_XPATH) if item.text))),
        languages=languages,
        buyer_types=[item.text.lower() for item in xml.all(BUYER_LEGAL_TYPE_XPATH) if item.text],
        contracting_bodies=list(_extract_contracting_bodies(xml)),
        eu_funds=[item.text for item in xml.all(EU_FUNDS_XPATH) if item.text],
        free_texts=free_texts,
        translated_free_texts=[
            text.lower()
            for text in _translate_to_english(free_texts, languages, translation_model)
        ],
    )
    LOGGER.debug(f"Extracted fields for notice {notice_id}: {fields}")
    return fields


def _extract_contracting_bodies(xml: Xml) -> Iterator[ContractingBody]:
    for buyer in xml.children(BUYER_XPATH):
        name = buyer.first(BUYER_NAME_XPATH) or ''
        town = buyer.first(BUYER_TOWN_XPATH) or ''
        postal_code = buyer.first(BUYER_POSTAL_CODE_XPATH) or ''
        country = parse_country(buyer.first(BUYER_COUNTRY_XPATH) or '')
        country = "uk" if country == "gb" else country
        yield ContractingBody(name=name, town=town, postal_code=postal_code, country=country)


def parse_countries(values: Iterator[str]) -> Iterator[str]:
    for value in values:
        if value not in BAD_COUNTRY_CODES:
            yield parse_country(value)


def parse_country(value: str) -> str:
    code = coco.convert(names=value, to='ISO2')
    code = code.lower() if code != "not found" else value[:2].lower()
    return "gb" if code == "uk" else code


def _extract_free_text(xml: Xml) -> list[str]:
    return list(set(filter(None, (text.strip() for text in xml.all(FREE_TEXT_XPATH) if not is_numeric(text)))))


def is_numeric(text: str) -> bool:
    return ''.join(e for e in text if e.isalnum()).isnumeric()


def _translate_to_english(texts: list[str], languages: list[str], translation_model: TranslationModel) -> Iterator[str]:
    if "eng" in languages:
        yield from texts
    else:
        for text in texts:
            encoded_input = translation_model.tokenizer([text], return_tensors="pt", padding=True, truncation=True)
            output = translation_model.model.generate(**encoded_input)
            yield translation_model.tokenizer.batch_decode(output, skip_special_tokens=True)[0]
