import logging
from logging import Logger

from config import APP_CONFIG


def logger(name: str) -> Logger:
    logging.basicConfig(format="[%(asctime)s] %(name)20s  %(levelname)8s  %(message)s")
    logger_ = logging.getLogger(name[len(name) - 20:])
    logger_.setLevel(getattr(logging, APP_CONFIG.log_level.upper()))
    return logger_
