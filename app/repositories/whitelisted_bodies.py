import psycopg2
from boto3_type_annotations.ssm import Client as SsmClient

from config import DB_CONFIG, WHITELISTED_BODY_CONFIG
from services.extraction import ContractingBody
from utils import logging

LOGGER = logging.logger(__name__)
DB_PASSWORD = ""


def whitelist_score(ssm: SsmClient, body: ContractingBody) -> float:
    global DB_PASSWORD
    if not DB_PASSWORD:
        ssm_response = ssm.get_parameter(Name=DB_CONFIG.password_ssm_parameter, WithDecryption=True)
        DB_PASSWORD = ssm_response['Parameter']['Value']
    conn = psycopg2.connect(database=DB_CONFIG.name, user=DB_CONFIG.username, password=DB_PASSWORD,
                            host=DB_CONFIG.host, port=DB_CONFIG.port)
    with conn.cursor() as cur:
        # similarity() is case-insensitive
        cur.execute(f"""
            SELECT max(
                similarity(official_name, '{body.name}') * {WHITELISTED_BODY_CONFIG.name_weight}
                + similarity(town, '{body.town}') * {WHITELISTED_BODY_CONFIG.town_weight}
                + similarity(postal_code, '{body.postal_code}') * {WHITELISTED_BODY_CONFIG.postal_code_weight}
            ) as score
            FROM {DB_CONFIG.whitelisted_bodies_table}
            WHERE country = '{body.country.lower()}'
        """)
        return cur.fetchone()[0]
