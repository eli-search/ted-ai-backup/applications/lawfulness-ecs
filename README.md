# TEDAI Lawfulness ECS

This repository contains the task that process notices using lawfulness rules. This task is run in an ECS cluster.

## Install requirements

```shell
python3 -m pip install -r requirements.txt
python3 -m pip install -r requirements-dev.txt
```

It is recommended to install these dependencies in a virtual environment.

## Run the task locally

```shell
export $(cat local/.env | cut -d# -f1 | xargs)
docker compose -f local/docker-compose.yml up localstack postgres --wait
python3 local/generate_company_whitelist_script.py
bash -x local/init_postgres.sh
bash -x local/init_localstack.sh

# Then run one of these commands: 
MODE=task python app/main.py
docker compose -f local/docker-compose.yml up --build task
```

## Run the API locally

```shell
export $(cat local/.env | cut -d# -f1 | xargs)
docker compose -f local/docker-compose.yml up localstack postgres --wait
python3 local/generate_company_whitelist_script.py
bash -x local/init_postgres.sh

# Then run one of these commands: 
MODE=api python app/main.py
docker compose -f local/docker-compose.yml up --build api -d
```

API is available at `localhost:8080`.

## Resource configuration

To run this task, the AWS environment must be configured.

### Create necessary PostgreSQL tables and extensions

- Locally, run `python local/generate_company_whitelist_script.py` to generate the init script
- Start an AWS WorkSpace with Amazon Linux
- In the WorkSpace,
    - Install
      psql: `sudo yum update -y && sudo amazon-linux-extras enable postgresql14 && sudo yum install postgresql -y`
    - Copy `local/resources/whitelisted_companies.sql`
    - Run the script in the RDS database (see `local/init_postgres.sh` to see what psql command must be run)
