#!/usr/bin/env bash

set -euo pipefail

export $(cat local/.env | cut -d# -f1 | xargs)
script_path=local/resources/whitelisted_companies.sql
PGPASSWORD="$DB_LOCAL_PASSWORD" psql --user "$DB_USERNAME" --host "$DB_HOST" --port "$DB_PORT" --dbname "$DB_NAME" -f $script_path
