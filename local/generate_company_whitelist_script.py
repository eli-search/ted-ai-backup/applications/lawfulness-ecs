import logging
from pathlib import Path

import pandas as pd
from pandas import Series
from unidecode import unidecode

logging.basicConfig(level=logging.INFO)

SCRIPT_FOLDER = Path(__file__).parent
INPUT_PATH = SCRIPT_FOLDER / "resources" / "Contracting_body_list published_white.xlsx"
OUTPUT_PATH = SCRIPT_FOLDER / "resources" / "whitelisted_companies.sql"

ENV_VARIABLES = {
    line.split("=", 2)[0]: line.split("=", 2)[1]
    for line in (SCRIPT_FOLDER / ".env").read_text().splitlines()
    if line
}
TABLE_NAME = ENV_VARIABLES["WHITELISTED_BODIES_TABLE_NAME"]
OFFICIAL_NAME_COLUMN = "Contracting_body_officialname"
TOWN_COLUMN = "Contracting_body__town"
POSTAL_CODE_COLUMN = "Contracting_body__postalcode"
COUNTY_COLUMN = "Contracting_body__country"


def main():
    OUTPUT_PATH.write_text((
            recreate_extensions_query("pg_trgm")
            + recreate_table_query()
            + content_insertion_query()
            + "\n"
    ))
    logging.info("Done.")


def recreate_extensions_query(name: str) -> str:
    return f"DROP EXTENSION IF EXISTS {name};\nCREATE EXTENSION {name};\n"


def recreate_table_query() -> str:
    return f"""DROP TABLE IF EXISTS {TABLE_NAME};
    CREATE TABLE {TABLE_NAME} (
        id            SERIAL PRIMARY KEY,
        official_name TEXT,
        town          TEXT,
        postal_code   TEXT,
        country       VARCHAR(2)
    );
    """


def content_insertion_query() -> str:
    logging.info("Open '%s'...", INPUT_PATH)
    df = pd.read_excel(str(INPUT_PATH))
    logging.info("Preprocess obtained dataframe...")
    df[OFFICIAL_NAME_COLUMN] = df[OFFICIAL_NAME_COLUMN].apply(preprocess_text)
    df[TOWN_COLUMN] = df[TOWN_COLUMN].apply(preprocess_text)
    df[POSTAL_CODE_COLUMN] = df[POSTAL_CODE_COLUMN].apply(preprocess_text)
    df[COUNTY_COLUMN] = df[COUNTY_COLUMN].apply(preprocess_text)
    logging.info("Generate final insertion query...")
    data = "\n".join(df.apply(whitelisted_body_insert_query, axis=1))
    return f"COPY {TABLE_NAME} (official_name, town, postal_code, country) FROM stdin (DELIMITER '$');\n{data}\n\\."


def preprocess_text(text: str | float) -> str:
    if pd.isna(text):
        return ""
    return unidecode(str(text).lower().strip())


def whitelisted_body_insert_query(body: Series):
    official_name = body[OFFICIAL_NAME_COLUMN]
    town = body[TOWN_COLUMN]
    postal_code = body[POSTAL_CODE_COLUMN]
    country = body[COUNTY_COLUMN]
    return f'{official_name}${town}${postal_code}${country}'


if __name__ == '__main__':
    main()
